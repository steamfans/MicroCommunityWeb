(function (vc) {
    vc.extends({
        data: {
            editWorkInfo: {
                workId: '',
                workName: '',
                workTypes: [],
                wtId: '',
                workCycle: '',
                startTime: '',
                endTime: '',
                staffs: [],
                copyStaffs: [],
                pathUrl: '',
                content: '',
                period: '',
                months: [],
                days: [],
                workdays: [],
                hours: '',
                beforeTime: '',
                periodMonth: '',
                periodDay: '',
                periodWorkday: '',
                communityId: ''
            }
        },
        _initMethod: function () {
            $that.editWorkInfo.workId = vc.getParam('workId');
            $that.editWorkInfo.hours = vc.getParam('hours');
            $that.editWorkInfo.beforeTime = vc.getParam('beforeTime');
            if (vc.getParam('periodMonth')) {
                let monthArray = [];
                vc.getParam('periodMonth').split("%2C").forEach(item => {
                    monthArray.push(item);
                })
                $that.editWorkInfo.months = monthArray;
            }
            if (vc.getParam('periodDay')) {
                let dayArray = [];
                vc.getParam('periodDay').split("%2C").forEach(item => {
                    dayArray.push(item);
                })
                $that.editWorkInfo.days = dayArray;
            }
            if (vc.getParam('periodWorkday')) {
                let workDayArray = [];
                vc.getParam('periodWorkday').split("%2C").forEach(item => {
                    workDayArray.push(item);
                })
                $that.editWorkInfo.workdays = workDayArray;
            }
            $that._loadWorkPool();
            $that._loadTaskStaff();
            $that._loadWorkCopy();
            $that._listWorkTypes();
            $that._loadWorkCycle();
            $that._initEditWorkDateInfo();
            vc.emit('textarea', 'init', $that.editWorkInfo);
            vc.component.editWorkInfo.content = vc.component.editWorkInfo.content.trim();
        },
        _initEvent: function () {
            vc.on('editWork', 'notifyFile', function (_param) {
                $that.editWorkInfo.pathUrl = _param.realFileName;
            })
        },
        methods: {
            _initEditWorkDateInfo: function () {
                $('.editWorkStartTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.editWorkStartTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".editWorkStartTime").val();
                        vc.component.editWorkInfo.startTime = value;
                    });
                $('.editWorkEndTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.editWorkEndTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".editWorkEndTime").val();
                        var start = Date.parse(new Date(vc.component.editWorkInfo.startTime))
                        var end = Date.parse(new Date(value))
                        if (start - end >= 0) {
                            vc.toast("结束时间必须大于开始时间！")
                            $(".editWorkEndTime").val('')
                        } else {
                            vc.component.editWorkInfo.endTime = value;
                        }
                    });
                //防止多次点击时间插件失去焦点
                document.getElementsByClassName('form-control editWorkStartTime')[0].addEventListener('click', myfunc)

                function myfunc(e) {
                    e.currentTarget.blur();
                }

                document.getElementsByClassName("form-control editWorkEndTime")[0].addEventListener('click', myfunc)

                function myfunc(e) {
                    e.currentTarget.blur();
                }
            },
            editWorkValidate() {
                return vc.validate.validate({
                    editWorkInfo: $that.editWorkInfo
                }, {
                    'editWorkInfo.workName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标题不能为空"
                        }
                    ],
                    'editWorkInfo.wtId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型不能为空"
                        }
                    ],
                    'editWorkInfo.workCycle': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标识不能为空"
                        }
                    ],
                    'editWorkInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        }
                    ],
                    'editWorkInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        }
                    ],
                    'editWorkInfo.hours': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小时数不能为空"
                        }
                    ],
                    'editWorkInfo.content': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作单内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "50",
                            errInfo: "工作单内容不能超过50"
                        }
                    ]
                });
            },
            saveWorkPool: function () {
                if (!$that.editWorkValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if (vc.component.editWorkInfo.staffs == null || vc.component.editWorkInfo.staffs == undefined
                    || vc.component.editWorkInfo.staffs == '' || vc.component.editWorkInfo.staffs == [] || vc.component.editWorkInfo.staffs.length < 1) {
                    vc.toast("请选择处理人！");
                    return;
                }
                if (vc.component.editWorkInfo.copyStaffs == null || vc.component.editWorkInfo.copyStaffs == undefined
                    || vc.component.editWorkInfo.copyStaffs == '' || vc.component.editWorkInfo.copyStaffs == [] || vc.component.editWorkInfo.copyStaffs.length < 1) {
                    vc.toast("请选择抄送人！");
                    return;
                }
                if (vc.component.editWorkInfo.workCycle == '2002' && vc.component.editWorkInfo.period == '2020022') {
                    if (vc.component.editWorkInfo.months == null || vc.component.editWorkInfo.months == []
                        || vc.component.editWorkInfo.months == undefined || vc.component.editWorkInfo.months.length < 1) {
                        vc.toast("月不能为空！");
                        return;
                    }
                    if (vc.component.editWorkInfo.days == null || vc.component.editWorkInfo.days == []
                        || vc.component.editWorkInfo.days == undefined || vc.component.editWorkInfo.days.length < 1) {
                        vc.toast("日不能为空！");
                        return;
                    }
                }
                if (vc.component.editWorkInfo.workCycle == '2002') {
                    if (vc.component.editWorkInfo.period == null || vc.component.editWorkInfo.period == undefined || vc.component.editWorkInfo.period == '') {
                        vc.toast("请选择任务周期");
                        return;
                    }
                }
                if (vc.component.editWorkInfo.workCycle == '2002' && vc.component.editWorkInfo.period == '2020023') {
                    if (vc.component.editWorkInfo.workdays == null || vc.component.editWorkInfo.workdays == []
                        || vc.component.editWorkInfo.workdays == '' || vc.component.editWorkInfo.workdays == undefined || vc.component.editWorkInfo.workdays.length < 1) {
                        vc.toast("周不能为空！");
                        return;
                    }
                }
                if (vc.component.editWorkInfo.content == "<p></p>") {
                    vc.toast("工作单内容不能为空");
                    return;
                }
                vc.http.apiPost(
                    '/work.updateWorkPool',
                    JSON.stringify($that.editWorkInfo), {
                        emulateJSON: true
                    },
                    function (json, res) {
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            vc.toast('修改成功');
                            vc.goBack();
                            return;
                        } else {
                            vc.toast(_json.msg);
                        }
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            _listWorkTypes: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 100,
                        communityId: vc.getCurrentCommunity().communityId
                    }
                };
                //发送get请求
                vc.http.apiGet('/workType.listWorkType',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.editWorkInfo.workTypes = _json.data;
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _chooseWorkStaff: function () {
                vc.emit('selectStaff', 'openStaff', {
                    call: function (_staff) {
                        if ($that.editWorkInfo.staffs.length > 0) {
                            for (let index = 0; index < $that.editWorkInfo.staffs.length; index++) {
                                var item = $that.editWorkInfo.staffs[index];
                                if (_staff.staffId === item.staffId) {
                                    return;
                                } else {
                                    continue;
                                }
                            }
                            $that.editWorkInfo.staffs.push(_staff);
                        } else {
                            $that.editWorkInfo.staffs.push(_staff);
                        }
                    }
                });
            },
            _deleteWorkStaff: function (_staff) {
                let _staffs = [];
                $that.editWorkInfo.staffs.forEach(item => {
                    if (_staff.staffId != item.staffId) {
                        _staffs.push(item);
                    }
                });
                $that.editWorkInfo.staffs = _staffs;
            },
            _chooseCopyWorkStaff: function () {
                vc.emit('selectStaff', 'openStaff', {
                    call: function (_staff) {
                        if ($that.editWorkInfo.copyStaffs.length > 0) {
                            for (let index = 0; index < $that.editWorkInfo.copyStaffs.length; index++) {
                                var item = $that.editWorkInfo.copyStaffs[index];
                                if (_staff.staffId === item.staffId) {
                                    return;
                                } else {
                                    continue;
                                }
                            }
                            $that.editWorkInfo.copyStaffs.push(_staff);
                        } else {
                            $that.editWorkInfo.copyStaffs.push(_staff);
                        }
                    }
                });
            },
            _deleteCopyWorkStaff: function (_staff) {
                let _staffs = [];
                $that.editWorkInfo.copyStaffs.forEach(item => {
                    if (_staff.staffId != item.staffId) {
                        _staffs.push(item);
                    }
                });
                $that.editWorkInfo.copyStaffs = _staffs;
            },
            _loadWorkPool: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 1,
                        workId: $that.editWorkInfo.workId
                    }
                };
                //发送get请求
                vc.http.apiGet('/work.queryStartWork',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        vc.copyObject(_json.data[0], $that.editWorkInfo);
                        vc.emit('textarea', 'setText', _json.data[0].content);
                        if (_json.data[0].pathUrl) {
                            vc.emit('uploadFile', 'notifyVedio', _json.data[0].pathUrl);
                        }
                        $that._loadWorkCycle();
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _loadTaskStaff: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 100,
                        workId: $that.editWorkInfo.workId
                    }
                };
                //发送get请求
                vc.http.apiGet('/work.listWorkTask',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.editWorkInfo.staffs = _json.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _loadWorkCopy: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 100,
                        workId: $that.editWorkInfo.workId
                    }
                };
                //发送get请求
                vc.http.apiGet('/work.listWorkCopy',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.editWorkInfo.copyStaffs = _json.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _loadWorkCycle: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 1,
                        workId: $that.editWorkInfo.workId
                    }
                };
                //发送get请求
                vc.http.apiGet('/workCycle.listWorkCycle',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        //$that.editWorkInfo.copyStaffs = _json.data;
                        $that.editWorkInfo.period = _json.data[0].period;
                        if (_json.data[0].periodMonth) {
                            $that.editWorkInfo.months = _json.data[0].periodMonth.split(',');
                        }
                        if (_json.data[0].periodDay) {
                            $that.editWorkInfo.days = _json.data[0].periodDay.split(',');
                        }
                        if (_json.data[0].workdays) {
                            $that.editWorkInfo.workdays = _json.data[0].workdays.split(',');
                        }
                        $that.editWorkInfo.hours = _json.data[0].hours;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            }
        }
    });
})(window.vc);