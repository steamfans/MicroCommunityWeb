(function (vc) {
    vc.extends({
        data: {
            addWorkInfo: {
                workName: '',
                workTypes: [],
                wtId: '',
                workCycle: '',
                startTime: '',
                endTime: '',
                staffs: [],
                copyStaffs: [],
                pathUrl: '',
                content: '',
                period: '',
                months: [],
                days: [],
                workdays: [],
                hours: '',
                communityId: ''
            }
        },
        _initMethod: function () {
            $that._listWorkTypes();
            $that._initAddWorkDateInfo();
            $that.addWorkInfo.startTime = vc.dateTimeFormat(new Date().getTime());
            $that.addWorkInfo.endTime = vc.dateTimeFormat(vc.addOneDay(new Date().getTime()).getTime());
            $that.addWorkInfo.communityId = vc.getCurrentCommunity().communityId;
            vc.emit('textarea', 'init', $that.addWorkInfo);
            vc.component.addWorkInfo.content = vc.component.addWorkInfo.content.trim();
        },
        _initEvent: function () {
            vc.on('addWork', 'notifyFile', function (_param) {
                $that.addWorkInfo.pathUrl = _param.realFileName;
            })
        },
        methods: {
            _initAddWorkDateInfo: function () {
                $('.addWorkStartTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.addWorkStartTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".addWorkStartTime").val();
                        vc.component.addWorkInfo.startTime = value;
                    });
                $('.addWorkEndTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.addWorkEndTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".addWorkEndTime").val();
                        var start = Date.parse(new Date(vc.component.addWorkInfo.startTime))
                        var end = Date.parse(new Date(value))
                        if (start - end >= 0) {
                            vc.toast("结束时间必须大于开始时间！")
                            $(".addWorkEndTime").val('')
                        } else {
                            vc.component.addWorkInfo.endTime = value;
                        }
                    });
                //防止多次点击时间插件失去焦点
                document.getElementsByClassName('form-control addWorkStartTime')[0].addEventListener('click', myfunc)

                function myfunc(e) {
                    e.currentTarget.blur();
                }

                document.getElementsByClassName("form-control addWorkEndTime")[0].addEventListener('click', myfunc)

                function myfunc(e) {
                    e.currentTarget.blur();
                }
            },
            addWorkValidate() {
                return vc.validate.validate({
                    addWorkInfo: $that.addWorkInfo
                }, {
                    'addWorkInfo.workName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标题不能为空"
                        }
                    ],
                    'addWorkInfo.wtId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作单类型不能为空"
                        }
                    ],
                    'addWorkInfo.workCycle': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作单标识不能为空"
                        }
                    ],
                    'addWorkInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        }
                    ],
                    'addWorkInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        }
                    ],
                    'addWorkInfo.hours': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小时数不能为空"
                        }
                    ],
                    'addWorkInfo.content': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作单内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "50",
                            errInfo: "工作单内容不能超过50"
                        }
                    ]
                });
            },
            saveWorkPool: function () {
                if (!$that.addWorkValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if (vc.component.addWorkInfo.staffs == null || vc.component.addWorkInfo.staffs == undefined
                    || vc.component.addWorkInfo.staffs == '' || vc.component.addWorkInfo.staffs == [] || vc.component.addWorkInfo.staffs.length < 1) {
                    vc.toast("请选择处理人！");
                    return;
                }
                if (vc.component.addWorkInfo.copyStaffs == null || vc.component.addWorkInfo.copyStaffs == undefined
                    || vc.component.addWorkInfo.copyStaffs == '' || vc.component.addWorkInfo.copyStaffs == [] || vc.component.addWorkInfo.copyStaffs.length < 1) {
                    vc.toast("请选择抄送人！");
                    return;
                }
                if (vc.component.addWorkInfo.workCycle == '2002' && vc.component.addWorkInfo.period == '2020022') {
                    if (vc.component.addWorkInfo.months == null || vc.component.addWorkInfo.months == []
                        || vc.component.addWorkInfo.months == undefined || vc.component.addWorkInfo.months.length < 1) {
                        vc.toast("月不能为空！");
                        return;
                    }
                    if (vc.component.addWorkInfo.days == null || vc.component.addWorkInfo.days == []
                        || vc.component.addWorkInfo.days == undefined || vc.component.addWorkInfo.days.length < 1) {
                        vc.toast("日不能为空！");
                        return;
                    }
                }
                if (vc.component.addWorkInfo.workCycle == '2002') {
                    if (vc.component.addWorkInfo.period == null || vc.component.addWorkInfo.period == undefined || vc.component.addWorkInfo.period == '') {
                        vc.toast("请选择任务周期");
                        return;
                    }
                }
                if (vc.component.addWorkInfo.workCycle == '2002' && vc.component.addWorkInfo.period == '2020023') {
                    if (vc.component.addWorkInfo.workdays == null || vc.component.addWorkInfo.workdays == []
                        || vc.component.addWorkInfo.workdays == '' || vc.component.addWorkInfo.workdays == undefined || vc.component.addWorkInfo.workdays.length < 1) {
                        vc.toast("周不能为空！");
                        return;
                    }
                }
                if (vc.component.addWorkInfo.content == "<p></p>") {
                    vc.toast("工作单内容不能为空");
                    return;
                }
                vc.http.apiPost(
                    '/work.saveWorkPool',
                    JSON.stringify($that.addWorkInfo), {
                        emulateJSON: true
                    },
                    function (json, res) {
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            vc.toast('添加成功');
                            vc.goBack();
                            return;
                        } else {
                            vc.toast(_json.msg);
                        }
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            _listWorkTypes: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 100,
                        communityId: vc.getCurrentCommunity().communityId
                    }
                };
                //发送get请求
                vc.http.apiGet('/workType.listWorkType',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.addWorkInfo.workTypes = _json.data;
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _chooseWorkStaff: function () {
                vc.emit('selectStaff', 'openStaff', {
                    call: function (_staff) {
                        if ($that.addWorkInfo.staffs.length > 0) {
                            for (let index = 0; index < $that.addWorkInfo.staffs.length; index++) {
                                var item = $that.addWorkInfo.staffs[index];
                                if (_staff.staffId === item.staffId) {
                                    return;
                                } else {
                                    continue;
                                }
                            }
                            $that.addWorkInfo.staffs.push(_staff);
                        } else {
                            $that.addWorkInfo.staffs.push(_staff);
                        }
                    }
                });
            },
            _deleteWorkStaff: function (_staff) {
                let _staffs = [];
                $that.addWorkInfo.staffs.forEach(item => {
                    if (_staff.staffId != item.staffId) {
                        _staffs.push(item);
                    }
                });
                $that.addWorkInfo.staffs = _staffs;
            },
            _deleteCopyStaff: function (_staff) {
                let _staffs = [];
                $that.addWorkInfo.copyStaffs.forEach(item => {
                    if (_staff.staffId != item.staffId) {
                        _staffs.push(item);
                    }
                });
                $that.addWorkInfo.copyStaffs = _staffs;
            },
            _chooseCopyWorkStaff: function () {
                vc.emit('selectStaff', 'openStaff', {
                    call: function (_staff) {
                        if ($that.addWorkInfo.copyStaffs.length > 0) {
                            for (let index = 0; index < $that.addWorkInfo.copyStaffs.length; index++) {
                                var item = $that.addWorkInfo.copyStaffs[index];
                                if (_staff.staffId === item.staffId) {
                                    return;
                                } else {
                                    continue;
                                }
                            }
                            $that.addWorkInfo.copyStaffs.push(_staff);
                        } else {
                            $that.addWorkInfo.copyStaffs.push(_staff);
                        }
                    }
                });
            },
            _deleteCopyWorkStaff: function (_staff) {
                let _staffs = [];
                $that.addWorkInfo.copyStaffs.forEach(item => {
                    if (_staff.staffId != item.staffId) {
                        _staff.push(item);
                    }
                });
                $that.addWorkInfo.copyStaffs = _staffs;
            }
        }
    });
})(window.vc);